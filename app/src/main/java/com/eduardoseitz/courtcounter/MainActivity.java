package com.eduardoseitz.courtcounter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // Local variables
    int _teamAScore = 0;
    int _teamBScore = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Assing layout elements
        final TextView teamAScoreText = findViewById(R.id.score_text_view_a);
        final TextView teamBScoreText = findViewById(R.id.score_text_view_b);

        final Button teamAButtons[] = new Button[]{findViewById(R.id.add_one_point_button_a), findViewById(R.id.add_two_points_button_a), findViewById(R.id.add_three_points_button_a)};
        final Button teamBButtons[] = new Button[]{findViewById(R.id.add_one_point_button_b), findViewById(R.id.add_two_points_button_b), findViewById(R.id.add_three_points_button_b)};
        final Button resetButton = findViewById(R.id.reset_button);

        // Add score to team A buttons
        teamAButtons[0].setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                _teamAScore = IncreaseScore(_teamAScore,1, teamAScoreText);
            }
        });

        teamAButtons[1].setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                _teamAScore = IncreaseScore(_teamAScore,2, teamAScoreText);
            }
        });

        teamAButtons[2].setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                _teamAScore = IncreaseScore(_teamAScore,3, teamAScoreText);
            }
        });

        // Add score to team B buttons
        teamBButtons[0].setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                _teamBScore = IncreaseScore(_teamBScore,1, teamBScoreText);
            }
        });

        teamBButtons[1].setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                _teamBScore = IncreaseScore(_teamBScore,2, teamBScoreText);
            }
        });

        teamBButtons[2].setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                _teamBScore = IncreaseScore(_teamBScore,3, teamBScoreText);
            }
        });

        // Reset score button
        resetButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                ResetScore(teamAScoreText, teamBScoreText);
            }
        });
    }

    // Increase score
    int IncreaseScore(int score, int increase, TextView scoreText)
    {
        score += increase;
        scoreText.setText(Integer.toString(score));
        return score;
    }

    // Reset score
    void ResetScore(TextView textA, TextView textB)
    {
        _teamAScore = 0;
        _teamBScore = 0;

        textA.setText(Integer.toString(_teamAScore));
        textB.setText(Integer.toString(_teamBScore));
    }
}
